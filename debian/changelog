nitrogen (1.6.0-2) experimental; urgency=medium

  * Set the maintainer to Stefan Schörghofer <amd1212@4md.gr>
  * Updated Website URL
  * Updated Vcs
  * Updated debian/watch file
  * Added .gitlab-ci.yml

 -- Stefan Schörghofer <amd1212@4md.gr>  Fri, 25 Jan 2018 14:10:15+0100

nitrogen (1.6.0-1) unstable; urgency=medium

  * QA upload
  * Set the maintainer to Debian QA Group
  * New upstream release (Closes: #796980)
  * Increase the compat level to 10
    - Build depend on dh > 10
    - Remove build dependency on autotools-dev, autoconf, autopoint,
      and automake
  * Remove build dependency on dpkg-deb (>= 1.16.1~)
  * Bump standards version to 3.9.8, the following changes were needed
    - Remove obsolete menu file
  * Enable hardened build
  * Simplify debian/rules
  * Drop old pathches
    - fix_FTBFS_binutils-gold.patch, no longer needed
    - add_desktop_file.patch, merged upstream
  * Fix spelling on manpage
    - spelling-fixes.patch
  * Enable build on !linux, thanks Christoph Egger
    <christoph@debian.org> for the patch (Closes: #794901)
  * Package upstream's git log as changelog, as the Changelog in the source
    is very outdated
  * Add README.source
  * Packaging is now maintained on a git repo under collab-maint
  * Add Vcs fields to debian/control
  * Add pristine-tar to the git repo
  * Add gbp.conf

 -- gustavo panizzo <gfa@zumbi.com.ar>  Mon, 14 Nov 2016 11:17:30 +0800

nitrogen (1.5.2-2) unstable; urgency=low

  * Bump standards version.
  * Readd series file (I don't know when this got dropped), to fix FTBFS.
    (Closes: #713669)
  * Add upstream change to include .desktop file (Closes: #709215).
  * Add build-arch and build-indep targets.
  * Enable hardening options.

 -- Nico Golde <nion@debian.org>  Sat, 22 Jun 2013 16:32:26 +0200

nitrogen (1.5.2-1) unstable; urgency=low

  * New upstream version.
  * Bump standards version.
  * debian/control
    - add "Build-Depends: autoconf, autopoint, automake"
  * debian/rules
    - run autopoint, aclocal and autoreconf
  * debian/source/format
    - use "3.0 (quilt)"
  * debian/patches
    - add fix_FTBFS_binutils-gold.patch to specify X11 lib.
  * Thanks Hideki Yamane!

 -- Nico Golde <nion@debian.org>  Sun, 08 Jan 2012 13:25:12 +0200

nitrogen (1.5.1-1) unstable; urgency=low

  * New upstream release.
  * Bumping standards version, no changes needed.

 -- Nico Golde <nion@debian.org>  Wed, 03 Mar 2010 21:03:00 +0100

nitrogen (1.5-1) unstable; urgency=low

  * New upstream release
    - Fix gui crash on certain xinerama setups (Closes: #550673).
  * Bump policy version, no changes needed.
  * Minor copyright updates.

 -- Nico Golde <nion@debian.org>  Tue, 26 Jan 2010 15:37:19 +0100

nitrogen (1.4-2) unstable; urgency=low

  * Workaround libpng bug by moving include for png.h to the top
    of Thumbview.cc (Closes: #527673).
  * debian/rules: Replace dh_clean -k bei dh_prep.
  * Bump policy version, no changes needed.
  * Minor copyright updates.

 -- Nico Golde <nion@debian.org>  Mon, 11 May 2009 16:17:46 +0200

nitrogen (1.4-1) unstable; urgency=low

  * New upstream release.
  * Bump standards version to 3.8.0.
  * Bump compat version to 7 and adapt debhelper dependency.
  * Added watch file.

 -- Nico Golde <nion@debian.org>  Mon, 11 Aug 2008 15:15:28 +0200

nitrogen (1.3-1) unstable; urgency=low

  * New upstream release:
    - Made the program not recurse when no directory argument is given, thus
      not loading your entire home dir when you least expect it!
    - Made thumbnail loading lazy. Only thumbnails that you see in the GUI are
      actually loaded into memory.

 -- Nico Golde <nion@debian.org>  Wed, 14 May 2008 16:03:35 +0200

nitrogen (1.2-1) unstable; urgency=low

  * Initial release (Closes: #425940).

 -- Nico Golde <nion@debian.org>  Sat, 03 May 2008 18:10:00 +0200

